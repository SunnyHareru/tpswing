package frames;

import better.domain.*;
import better.service.Personnel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class FramePrincipale extends JFrame {

    /**
	 *
	 */
	private Personnel p;
	private static final long serialVersionUID = 1L;
		JPanel panelMenu = new JPanel();
    JPanel panelList = new JPanel();
    JPanel panelAdd = new JPanel();
    JButton button_menu_add = new JButton("Ajouter Employés");
    JButton button_menu_list = new JButton("Liste Employés");
    JButton button_list_back = new JButton("Retour");
    JButton button_add_back = new JButton("Retour");
    JButton button_submit = new JButton("Submit");

    JTextField text_name = new JTextField();
    JTextField text_first_name = new JTextField();
    JTextField text_age = new JTextField();
    JTextField text_salary = new JTextField();
    JTextField text_entry = new JTextField();



    JComboBox combJob = new JComboBox();
    public int Job = (int) combJob.getSelectedIndex();

    JLabel nom = new JLabel("Nom");
    JLabel prenom = new JLabel("Prénom");
    JLabel age = new JLabel("Age");
    JLabel entry = new JLabel("Année d'entrée");
    JLabel metier = new JLabel("Métier");
    JLabel salaire = new JLabel("Salaire");


    public FramePrincipale() {


        super("Gestionnaire de Salariés");

        p = new Personnel();
        WindowListener l = new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        };

        addWindowListener(l);


        //Code Panel Menu

        panelMenu.add(button_menu_add);
        panelMenu.add(button_menu_list);
        setContentPane(panelMenu);
        setSize(920, 640);
        setVisible(true);

        //Code Panel Ajout

        combJob.addItem ("Manutentionnaire");
        combJob.addItem ("Representant");
        combJob.addItem ("Technicien");
        combJob.addItem ("Vendeur");
        combJob.addItem ("ManutentARisque");
        combJob.addItem ("TechARisque");
        combJob.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent arg0) {
	        }
          });



        panelAdd.setLayout(new FlowLayout(FlowLayout.LEADING));
        panelAdd.add(nom);
        panelAdd.add(text_name);
        text_name.setPreferredSize(new Dimension(880, 40));
        panelAdd.add(prenom);
        panelAdd.add(text_first_name);
        text_first_name.setPreferredSize(new Dimension(880, 40));
        panelAdd.add(age);
        panelAdd.add(text_age);
        text_age.setPreferredSize(new Dimension(880, 40));
        panelAdd.add(entry);
        panelAdd.add(text_entry);
        text_entry.setPreferredSize(new Dimension(880, 40));
        panelAdd.add(metier);
        panelAdd.add(combJob);
        combJob.setPreferredSize(new Dimension(880, 40));
        panelAdd.add(salaire);
        panelAdd.add(text_salary);
        text_salary.setPreferredSize(new Dimension(880, 40));

        panelAdd.add(button_add_back);
        panelAdd.add(button_submit);
        setContentPane(panelAdd);
        setSize(920, 640);
        setVisible(true);


        // Code Boutons
        button_menu_add.addActionListener(new FenetreAdd());
        button_menu_list.addActionListener(new FenetreTable());
        button_add_back.addActionListener(new FenetrePrincipale());
        button_list_back.addActionListener(new FenetrePrincipale());
        button_submit.addActionListener(new FenetreAdd() {
            public void actionPerformed(ActionEvent click){
                switch (Job) {
                    case '1':
                        p.ajouterEmploye(new better.domain.Manutentionnaire(text_name.getText(), text_first_name.getText(),
                                Integer.parseInt(text_age.getText()), text_entry.getText(), Integer.parseInt(text_salary.getText())));
                        break;
                    case '2':
                        p.ajouterEmploye(new better.domain.Representant(text_name.getText(), text_first_name.getText(),
                                Integer.parseInt(text_age.getText()), text_entry.getText(), Integer.parseInt(text_salary.getText())));
                        break;
                    case '3':
                        p.ajouterEmploye(new better.domain.Technicien(text_name.getText(), text_first_name.getText(),
                                Integer.parseInt(text_age.getText()), text_entry.getText(), Integer.parseInt(text_salary.getText())));
                        break;
                    case '4':
                        p.ajouterEmploye(new better.domain.Vendeur(text_name.getText(), text_first_name.getText(),
                                Integer.parseInt(text_age.getText()), text_entry.getText(), Integer.parseInt(text_salary.getText())));
                        break;
                    case '5':
                        p.ajouterEmploye(new better.domain.ManutARisque(text_name.getText(), text_first_name.getText(),
                                Integer.parseInt(text_age.getText()), text_entry.getText(), Integer.parseInt(text_salary.getText())));
                        break;
                    case '6':
                        p.ajouterEmploye(new better.domain.TechnARisque(text_name.getText(), text_first_name.getText(),
                                Integer.parseInt(text_age.getText()), text_entry.getText(), Integer.parseInt(text_salary.getText())));
                        break;
                    default:
                }}});


        //Code Panel Liste

        Object[][] data = p.savedTable();

        String[] names = {"Prenom", "Nom", "Age", "Metier", "Salaire"};

        JTable tableau = new JTable(data, names);
        panelList.add(tableau.getTableHeader(), BorderLayout.NORTH);
        panelList.add(tableau, BorderLayout.CENTER);
        panelList.setLayout(new BoxLayout(panelList,BoxLayout.PAGE_AXIS));
        panelList.add(button_list_back);
        setContentPane(panelList);
        setSize(920, 640);
        setVisible(true);

    }

    public class FenetreAdd implements ActionListener{
        public void actionPerformed(ActionEvent click) {
            FramePrincipale.this.PasserVersAdd();



        }
    }


    public void PasserVersTable(){
        this.setContentPane(this.panelList);
        this.revalidate();
    }

    public void PasserVersMenu(){
        this.setContentPane(this.panelMenu);
        this.revalidate();
    }

    public void PasserVersAdd(){
        this.setContentPane(this.panelAdd);
        this.revalidate();

    }






    public class FenetreTable implements ActionListener{
        public void actionPerformed(ActionEvent click) {
            FramePrincipale.this.PasserVersTable();
        }
    }


    class FenetrePrincipale implements ActionListener{
        public void actionPerformed(ActionEvent click) {
            FramePrincipale.this.PasserVersMenu();
        }
    }








}
