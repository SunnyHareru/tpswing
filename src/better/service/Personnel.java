package better.service;

import better.domain.Employee;

import java.util.ArrayList;
import java.util.List;

public class Personnel {

    private List<Employee> employees;
    private static final String PRINT_PATTERN = "%s gagne %.2f euros";

    public Personnel() {
        this.employees = new ArrayList<>();
        this.addList();
    }

    public void ajouterEmploye(Employee employee) {
        this.employees.add(employee);
    }

    public void afficherSalaires() {
        employees.forEach(employee -> System.out.println(String.format(PRINT_PATTERN, employee.getName(), employee.calculerSalaire())));
    }

    public double salaireMoyen() {
        double total = 0;
        for (Employee employee: employees) {
            total += employee.calculerSalaire();
        }
        return total / employees.size();
    }

    public Object[][] savedTable(){
        int nmbEmployes = employees.size();
        Object[][] resultat = new Object[nmbEmployes][5];
        int i = 0;
        for (Employee employee: employees) {
            resultat[i][0] = employee.getLastname();
            System.out.println(resultat[i][0]);
            resultat[i][1] = employee.getFirstname();
            resultat[i][2] = employee.getAge();
            resultat[i][3] = employee.getEntryYear();
            resultat[i][4] = employee.calculerSalaire();
            i++;
        }
        return resultat;
    }

    private void addList() {
        for (int i=0; i < 10; ++i) {
            this.ajouterEmploye(new better.domain.Vendeur("Pierre", "Henri", 25, "2017", 15));
        }

    }
}
